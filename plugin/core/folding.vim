set foldmethod=indent
" set foldlevel=2
set nofoldenable

function! FoldText()
    let line = getline(v:foldstart)
    let char = matchstr(&fillchars, 'fold:\zs.')

    let foldedlinecount = v:foldend - v:foldstart
    let foldedlinecounttext = '[' . foldedlinecount . ' lines]'
    "
    " expand tabs into spaces
    let onetab = strpart('          ', 0, &tabstop)
    let line = substitute(line, '\t', onetab, 'g')

    let line = strpart(line, 0, winwidth(0) - len(foldedlinecount))
    let fillcharcount = winwidth(0) - len(line) - len(foldedlinecounttext)

    return line . ' ' . repeat('—',fillcharcount - 8) . ' ' . foldedlinecounttext
endfunction
set foldtext=FoldText()

