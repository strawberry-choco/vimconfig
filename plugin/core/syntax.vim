" enable syntax highlighting
if has('syntax') && !exists('g:syntax_on')
  syntax enable
endif

" jump briefly to the matching bracket
set showmatch

" italic comments
hi Comment cterm=italic

