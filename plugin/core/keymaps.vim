" Map the leader key to SPACE
let mapleader="\<SPACE>"

" allow going to next/previous line with left and right arrow keys
set whichwrap=b,s,<,>,[,]

" proper backspace bahavior
set backspace=indent,eol,start

" allow saving as sudo
if !has('nvim')
    cmap w!! w !sudo tee > /dev/null %
else
    cmap w!! w suda://%
endif

" which key
call which_key#register('<Space>', "g:which_key_map")
nnoremap <silent> <leader> :<c-u>WhichKey '<Space>'<CR>
set timeoutlen=500
let g:which_key_map =  {}

"move to the split in the direction shown, or create a new split
nnoremap <leader>wh :call WinMove('h')<cr>
nnoremap <leader>wj :call WinMove('j')<cr>
nnoremap <leader>wk :call WinMove('k')<cr>
nnoremap <leader>wl :call WinMove('l')<cr>
let g:which_key_map.w = { 'name': '+window' }
let g:which_key_map.w.h = 'move window left'
let g:which_key_map.w.j = 'move window down'
let g:which_key_map.w.k = 'move window up'
let g:which_key_map.w.l = 'move window right'

function! WinMove(key)
  let t:curwin = winnr()
  exec "wincmd ".a:key
  if (t:curwin == winnr())
    if (match(a:key,'[jk]'))
      wincmd v
    else
      wincmd s
    endif
    exec "wincmd ".a:key
  endif
endfunction

let g:which_key_map['\'] = { 'name': '+internal' }
let g:which_key_map['\'].u = 'update plugins'
nnoremap <leader>\\u :PlugUpdate<CR>
let g:which_key_map['\'].U = 'update vim-plug'
nnoremap <leader>\\U :PlugUpgrade<CR>
let g:which_key_map['\']['?'] = 'list keymaps'
nnoremap <silent><nowait> <space>\\?  :<C-u>CocList maps<cr>
let g:which_key_map['\'].c = 'edit coc config'
nnoremap <leader>\\c :CocConfig<CR>

let g:which_key_map.p = { 'name': '+project' }
let g:which_key_map.p.t = 'open project file overview'
" nmap <leader>pt :Dirvish<CR>
nnoremap <silent><nowait> <space>pt  :<C-u>CocCommand explorer<cr>

let g:which_key_map.b = { 'name': '+buffer' }
let g:which_key_map.b.b = 'browse buffers'
nnoremap <silent><nowait> <space>bb  :<C-u>CocList buffers<cr>
let g:which_key_map.b.n = 'next buffer'
nnoremap <silent><nowait> <space>bn  :bnext<cr>
let g:which_key_map.b.p = 'previous buffer'
nnoremap <silent><nowait> <space>bp  :bprevious<cr>
let g:which_key_map.b.d = 'delete buffer'
nnoremap <silent><nowait> <space>bd  :bdelete<cr>

let g:which_key_map.f = { 'name': '+file' }
let g:which_key_map.f.f = 'browse files'
nnoremap <silent><nowait> <space>ff  :<C-u>CocList files<cr>
let g:which_key_map.f.s = 'search in all files'
nnoremap <silent><nowait> <space>fs  :<C-u>CocList grep<cr>
let g:which_key_map.f['.'] = { 'name': '+current' }
let g:which_key_map.f['.'].d = 'delete'
nnoremap <silent><nowait> <space>f.d  :call delete(expand('%')) \| bdelete!<CR>
let g:which_key_map.f['.'].s = 'search word'
nnoremap <silent><nowait> <space>f.s  :<C-u>CocList words<cr>

let g:which_key_map.c = { 'name': '+code' }
let g:which_key_map.c.a = 'code action'
nmap <silent> <leader>ca <Plug>(coc-codeaction)
let g:which_key_map.c.d = 'go to definition'
nmap <silent> <leader>cd <Plug>(coc-definition)
let g:which_key_map.c.r = 'find references'
nmap <silent> <leader>cr <Plug>(coc-references)
let g:which_key_map.c.j = 'go to implementation'
nmap <silent> <leader>cj <Plug>(coc-implementation)
let g:which_key_map.c.e = 'errors'
nnoremap <silent><nowait> <space>ce  :<C-u>CocList diagnostics<cr>
let g:which_key_map.c.o = 'current document symbols'
nnoremap <silent><nowait> <space>co  :<C-u>CocList outline<cr>
let g:which_key_map.c.e = 'workspace symbols'
nnoremap <silent><nowait> <space>cs  :<C-u>CocList -I symbols<cr>

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)
