set autoindent
set smartindent

" use four spaces instead of tabs
set smarttab
set shiftwidth=4
set tabstop=4
set expandtab

" unix line ending
set ff=unix

" remove trailing white space on save
function! StripTrailingWhitespace()
  if !&binary && &filetype != 'diff'
    normal mz
    normal Hmy
    %s/\s\+$//e
    normal 'yz<CR>
    normal `z
  endif
endfunction

augroup strip_whitespace
    autocmd!
    autocmd BufWritePre * :call StripTrailingWhitespace()
augroup END

" continue comment marker on next line
set formatoptions+=o

" same line joins for comments
if v:version > 703 || v:version == 703 && has('patch541')
    set formatoptions+=j
endif

