if &compatible
    " `:set nocp` has many side effects. Therefore this should be done
    " only when 'compatible' is set.
    set nocompatible
endif

" Disable useless default plugins
" see :help standard-plugin-list
let g:loaded_gzip = 1
let g:loaded_netrw = 1
let g:loaded_netrwPlugin = 1
" we use vim-matchup, which is better
let g:loaded_matchit = 1
" TODO: find a way to disable it, the doc does not say how to do it
"let g:loaded_spec = 1
let g:leaded_tar = 1
let g:loaded_tarPlugin = 1
let g:loaded_zip = 1
let g:loaded_zipPlugin = 1
if !has('nvim')
    let g:loaded_get_script = 1
    let g:loaded_vimball = 1
    let g:loaded_vimballPlugin = 1
endif

if has('nvim')
    call plug#begin('~/.local/share/nvim/plugged')
else
    call plug#begin('~/.vim/.plugged')
endif

Plug 'Shougo/neco-vim'
Plug 'tpope/vim-commentary'
Plug 'Raimondi/delimitMate'
Plug 'itchyny/lightline.vim'
Plug 'mhinz/vim-signify'
Plug 'sheerun/vim-polyglot'
Plug 'airblade/vim-rooter'
Plug 'liuchengxu/vim-which-key'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }}
Plug 'jeetsukumaran/vim-markology'
Plug 'mbbill/undotree', { 'on': 'UndotreeToggle' }
Plug 'andymass/vim-matchup'

Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'neoclide/coc-neco'
Plug 'neoclide/coc-tsserver', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-yaml', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-json', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-eslint', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-vetur', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-css', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-html', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-stylelint', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-jest', {'do': 'yarn install --frozen-lockfile'}
Plug 'neoclide/coc-lists'
Plug 'weirongxu/coc-explorer', {'do': 'yarn install --frozen-lockfile'}

" color schemes
Plug 'sonph/onehalf', {'rtp': 'vim/'}

if !has('nvim')
    " neovim already has cursor shapes, focus events and bracketed paste
    " mouse support can be enabled with `set mouse=a`
    Plug 'wincent/terminus'
else
    " execute commands as sudo because nvim does not support :w !sudo tee %
    " and others
    Plug 'lambdalisue/suda.vim'
endif

call plug#end()

