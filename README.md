# Vim Config by Chrunchyjesus
> (Neo)Vim just how I like it to be.

## Setup

1. Clone this repository to `.vim` or `.config/nvim` depending on whether you use vim or neovim.
1. Install [vim-plug](https://github.com/junegunn/vim-plug) and execute `:PlugInstall` inside (neo)vim.
1. See how to install [denite.nvim](https://github.com/Shougo/denite.nvim), which is used as the search backend.

## Directory Structure

- `plugin/core` contains the base configuration for vim, which is plugin independent. (Yeah, the name sucks, but whatever.)
- `plugin/` contains configurations for plugins.

